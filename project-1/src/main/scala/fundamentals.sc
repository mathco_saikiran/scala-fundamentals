
def plusOne(i: Int):Int= i + 1

//plusOne(1)

//Notice that the return type didn’t have to be specified, and parentheses around the short
//method body aren’t required


//Setting default values in functions

def makeConnection(timeout: Int = 5000, protocol:String = "http") {
  println("timeout = %d, protocol = %s".format(timeout, protocol))
  // more code here
}

makeConnection()
makeConnection(2000)
makeConnection(3000, "https")

//Return multiple values from a method

def getStockInfo = {
  // other code here ...
  ("NFLX", 100.00, 101.00) // this is a Tuple3
}

val (symbol, currentPrice, bidPrice) = getStockInfo


//Functional Programming

// functional programming techniques, includ‐
//ing the ability to define functions and pass them around as instances

//Using Function Literals (Anonymous Functions)

//so you can
//pass it into a method that takes a function, or to assign it to a variable


val x = List.range(1, 10)

val evens = x.filter((i: Int) => i % 2 == 0)

val evens1 = x.filter(i => i % 2 == 0) //still works

x.foreach((i:Int) => println(i))

//Finally, if a function literal consists of one statement that takes a single argument, you
//need not explicitly name and specify the argument, so the statement can finally be
//reduced to this:

x.foreach(println)


// Using Functions as Variables

//You want to pass a function around like a variable, just like you pass String, Int, and
//other variables


val double = (i: Int) => { i * 2 }

double(2) // 4
double(3) // 6

val list = List.range(1, 5)


list.map(double)

val f = (i: Int) => { i % 2 == 0 }

// implicit approach
val add1 = (x: Int, y: Int) => { x + y }
val add2 = (x: Int, y: Int) => x + y

// explicit approach
val add3: (Int, Int) => Int = (x,y) => { x + y }
val add4: (Int, Int) => Int = (x,y) => x + y


val addThenDouble: (Int, Int) => Int = (x,y) => {
  val a = x + y
  2 * a
}

//Scala’s Unit:

//The Scala Unit shown in these examples is similar to Java’s Void class.

def plusOne1(i: Int) = i + 1

//You want to define a method that takes a function as a parameter,


val plusOne = (i: Int) => { println(i+1) }

def exec(callback: Int => Unit) {
  // invoke the function we were given, giving it an Int parameter
  callback(1)
}


exec(plusOne)

//executeFunction(f:(Int, Int) => Boolean)
//exec(f:(String, Int, Double) => Seq[String])



//Defining a Method That Accepts a Simple Function


def executeAndPrint(f:(Int, Int) => Int, x: Int, y: Int) {
  val result = f(x, y)
  println(result)
}

//Using Closures
//As shown in the Solution, to create a closure in Scala, just define a function that refers
//to a variable that’s in the same scope as its declaration

var isOfVotingAge1 = (age: Int) => age >= 18
isOfVotingAge1(16) // false
isOfVotingAge1(20) // true

var votingAge = 18
var isOfVotingAge = (age: Int) => age >= votingAge

isOfVotingAge1(16) // false
isOfVotingAge1(20) // true


//Using Partially Applied Functions

//You want to eliminate repetitively passing variables into a function

val sum = (a: Int, b: Int, c: Int) => a + b + c

val f1 = sum(1, 2, _: Int)

f1(3)

//You want to return a function (algorithm) from a function or method

def saySomething(prefix: String) = (s: String) => {
  prefix + " " + s

}


val sayHello = saySomething("Hello")

sayHello("Al")

def greeting(language: String) = (name: String) => {
  language match {
    case "english" => "Hello, " + name
    case "spanish" => "Buenos dias, " + name
  }
}


val hello = greeting("english")

hello("Al")

//Reading from files

import scala.io.Source

val lines1 = Source.fromFile("/home/user/docker-intellij-scala/README.txt").getLines.toList
val lines2 = Source.fromFile("/Users/Al/.bash_profile").getLines.toArray




